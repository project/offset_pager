<?php


namespace Drupal\offset_pager\PathProcessor;

use Drupal\Core\PathProcessor\InboundPathProcessorInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Processes the inbound and outbound pager query.
 */
class OffsetPageProcessor implements InboundPathProcessorInterface {

    /**
     * {@inheritdoc}
     */
    public function processInbound($path, Request $request) {
        if (preg_match('/.*page=([0-9]+)$/', $request->getRequestUri(), $matches)) {
            if ($matches[1] == '1') {
                $response = new \Symfony\Component\HttpFoundation\RedirectResponse($path);
                $response->setStatusCode(301)->send();
            } else {
                $request->query->set('page', $matches[1]*1-1);
                $request->overrideGlobals();
            };
        }
        return $path;
    }
}